#! /usr/bin/env bash
# branch to use in xplat_utils;
#  * branch     : the current branch to use
#  * branch_file: used to override the value in branch
# Note: assumes the named branch exists (not checked)
branch="v1.0.1"
branch_file='zsubm_branch'
if [ -f "${branch_file}" ]; then
  # shellcheck disable=SC1090
  source "${branch_file}"
  printf "%-4s %s\n" "WARN" "using branch=${branch} (see file ${branch_file})"
else
  printf "%-4s %s\n" "OK" "using branch=${branch}"
fi

# at this point: branch is set to the branch to use

# -------------------
# indicates the start of a command
function log_start {
  tag="$1"
  printf "%-4s %-8s %s\n" "----" "starting:" "$tag"
}

# -------------------
# prints OK or WARN depending on the return code of the previous command
# note: assumes the git command is in the first part of a piped command
function log_check {
  rc="${PIPESTATUS[0]}"
  if [ "$rc" == 0 ]; then
    printf "%-4s %-8s\n" "   >" "rc=$rc"
  else
    printf "%-4s %-8s\n" "ERR" "rc=$rc"
  fi
}

# typically do "full" only once:
#   * any changes in tools/xplat_utils will be lost
#   * any untracked files will be lost
if [ "$1" = "full" ]; then
  log_start "rm prev"
  # remove the directory if it exists
  rm -rf tools/xplat_utils
  # remove the reference to xplat_utils in the repos git cache
  git rm -r --cached tools/xplat_utils | sed -e 's/^/  -- /'
  log_check

  log_start "clone"
  git submodule add --force git@bitbucket.org:arrizza-public/xplat-utils.git tools/xplat_utils | sed -e 's/^/  -- /'
  log_check

  log_start "init"
  git submodule update --init --recursive | sed -e 's/^/  -- /'
  log_check
fi

# update the submodule:
#   * to the given branch
#   * to the latest remote URL and other info for that repo
#   * to the latest commit in that branch
# show the current status and the current remote URL

log_start "sync"
git submodule sync --recursive | sed -e 's/^/  -- /'
log_check

#log_start "update"
#git submodule update --recursive --remote --rebase | sed -e 's/^/  -- /'
#log_check

log_start "checkout"
git submodule foreach "git checkout ${branch}" | sed -e 's/^/  -- /'
log_check

log_start "pull"
git submodule foreach 'git pull' | sed -e 's/^/  -- /'
log_check

log_start "status"
git submodule foreach 'git status' | sed -e 's/^/  -- /'
log_check

# confirm it is the correct remote URL
printf "%-4s %s\n" "----" "branch=${branch}"
git config --get remote.origin.url | sed -e 's/^/---- /'
