import serial

from tools.xplat_utils.os_specific import OsSpecific


# --------------------
## automate testing of the arduino app
class App:
    # --------------------
    ## constructor
    def __init__(self):
        ## reference to the serial port
        self._ser = None

    # --------------------
    ## initialize
    #
    # @return None
    def init(self):
        OsSpecific.init()
        if OsSpecific.os_name == 'macos':
            port = '/dev/tty.usbserial-113240'
        else:
            port = '/dev/ttyUSB0'
        self._ser = serial.Serial(port, 115_200)

    # --------------------
    ## terminate
    #
    # @return None
    def term(self):
        print()

        # close it
        self._ser.close()

    # --------------------
    ## send a message to the arduino and get the response back.
    # should just echo the incoming message
    #
    # @return None
    def run(self):
        for _ in range(3):  # 1 starting + 2 SOSs
            self._read_response()

        msg = 'hello world\n'
        for ch in msg:
            self._ser.write(ch.encode('utf-8'))

        for _ in range(2):  # one for hello world and one more SOS
            self._read_response()

    # --------------------
    ## Read response from arduino
    #
    # @return None
    def _read_response(self):
        while True:
            # get the next byte
            ch = self._ser.read(size=1)

            # if we got nothing from the Arduino,
            # it may have timed out, try again
            if not ch:
                continue

            if ch == 0x0D:
                continue

            # the response ends on a linefeed from the Arduino
            if ch == b'\r':
                break

            # print the character as a character (not a byte) on your PC
            print(ch.decode('utf-8'), end='')


# --------------------
### MAIN
def main():
    app = App()
    app.init()
    app.run()
    app.term()


main()
