* website: <https://arrizza.com/arduino-sos.html>
* installation: see <https://arrizza.com/setup-common.html>

## Summary

This project takes the LED blinking to a whole new level!

It blinks out the Morse Code pattern for SOS, that is 3 dots, 3 dashes, 3 dots.
see <https://en.wikipedia.org/wiki/Morse_code> for the full alphabet, letters, punctuation, etc.

## Setup

The setup for this project is very similar to that for [Arduino LED Blink](https://arrizza.com/arduino-led-blink)
for the LED.

## Main Loop

Most of the hard work is done by the helper functions.

First we set up some constants for the various delays between the units in the code. The definitions here are nearly a
direct copy from the descriptions in the wikipedia article

```cpp
static const uint16_t oneunit = 70;
static const uint16_t dit = (oneunit * 1);
static const uint16_t dah = (oneunit * 3);
static const uint16_t unitgap = oneunit;
static const uint16_t lettergap = (oneunit * 3);
static const uint16_t wordgap = (oneunit * 7);
```

Note: 70 ms seems a little slow, but it looks like it would be possible to tap a key that fast.

The codes for numeric (0-9) and alphabetic (a-z) are held in a couple of arrays:

```cpp
static const uint16_t numeric[10][6];
static const uint16_t alphabet[26][5];
```

The dits and dahs are held in each of these e.g. "s" and "o":

```cpp
{dit, dit, dit, 0,   0}, // s
{dah, dah, dah, 0,   0}, // o
```

These 3 functions are used to play a letter or a digit with appropriate delays between them:

```cpp
void play_letter(uint8_t ch);
void play_numeric(uint8_t ch);
// both of those use this to blink the LED for the right length of time: a dit or a dah 
void do_unit(uint16_t unit);
// delays between each unit of a dit or a dah
void do_unit_delay();
```

The ```play_string(char* str)``` function iterates through the given string and plays each character it finds, an
alphabetic character, a digit or a space.

The ```loop()``` plays the current buffer and then checks if the user has entered a string to play.

* it ignores any non-alphabetic characters
* it stores any alphanumerics and spaces
* if empty string is entered (e.g. cr/lf only) then the default string "SOS" is played 
