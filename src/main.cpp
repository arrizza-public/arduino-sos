// cppcheck-suppress missingInclude
#include "Arduino.h"
#include <ctype.h>

// if verbose, display timeouts, otherwise just show the letters
#define VERBOSE false

// see wikipedia https://en.wikipedia.org/wiki/Morse_code
// for the definition of a dot and dash:
//    1. the length of a dot is one unit
//    2. A dash is three units
//    3. the space between parts of the same letter is one unit
//    4. the space between letters is three units
//    5. the space between words is seven units.
static const uint16_t oneunit = 70;
static const uint16_t dit = (oneunit * 1);
static const uint16_t dah = (oneunit * 3);
static const uint16_t unitgap = oneunit;
static const uint16_t lettergap = (oneunit * 3);
static const uint16_t wordgap = (oneunit * 7);

// points to buffer to play
static char buffer[200];

// --------------------
// handle numeric chars 0 - 9
static const uint16_t numeric[10][6] = {
    { dah, dah, dah, dah, dah, 0 }, // 0
    { dit, dah, dah, dah, dah, 0 }, // 1
    { dit, dit, dah, dah, dah, 0 }, // 2
    { dit, dit, dit, dah, dah, 0 }, // 3
    { dit, dit, dit, dit, dah, 0 }, // 4
    { dit, dit, dit, dit, dit, 0 }, // 5
    { dah, dit, dit, dit, dit, 0 }, // 6
    { dah, dah, dit, dit, dit, 0 }, // 7
    { dah, dah, dah, dit, dit, 0 }, // 8
    { dah, dah, dah, dah, dit, 0 }, // 9
};

// --------------------
// handle alphabetic values a - z
static const uint16_t alphabet[26][5] = {
    { dit, dah, 0, 0, 0 }, // a
    { dah, dit, dit, dit, 0 }, // b
    { dah, dit, dah, dit, 0 }, // c
    { dah, dit, dit, 0, 0 }, // d
    { dit, 0, 0, 0, 0 }, // e
    { dit, dit, dah, dit, 0 }, // f
    { dah, dah, dit, 0, 0 }, // g
    { dit, dit, dit, dit, 0 }, // h
    { dit, dit, 0, 0, 0 }, // i
    { dit, dah, dah, dah, 0 }, // j
    { dah, dit, dah, 0, 0 }, // k
    { dit, dah, dit, dit, 0 }, // l
    { dah, dah, 0, 0, 0 }, // m
    { dah, dit, 0, 0, 0 }, // n
    { dah, dah, dah, 0, 0 }, // o
    { dit, dah, dah, dit, 0 }, // p
    { dah, dah, dit, dah, 0 }, // q
    { dit, dah, dit, 0, 0 }, // r
    { dit, dit, dit, 0, 0 }, // s
    { dah, 0, 0, 0, 0 }, // t
    { dit, dit, dah, 0, 0 }, // u
    { dit, dit, dit, dah, 0 }, // v
    { dit, dah, dah, 0, 0 }, // w
    { dah, dit, dit, dah, 0 }, // x
    { dah, dit, dah, dah, 0 }, // y
    { dah, dah, dit, dit, 0 }, // z
};

// --------------------
//! Arduino setup
// cppcheck-suppress unusedFunction
void setup()
{
    // open the serial port at 115200 bps
    Serial.begin(115200);

    Serial.println("starting");

    strcpy(buffer, "SOS");

    // initialize the digital pin as an output.
    // Pin 13 has an LED connected on most Arduino boards:
    pinMode(13, OUTPUT);
}

// --------------------

// print a character to the serial port
static void print_char(uint8_t ch)
{
    Serial.print((char)ch);
#if VERBOSE
    Serial.print(": ");
#endif
}

// --------------------
// print a space i.e. a gap between words
static void print_wordgap()
{
    print_char(' ');
#if VERBOSE
    Serial.println("-");
#endif
}

// --------------------
// flash the led for the given number of units
static void do_unit(uint16_t unit)
{
#if VERBOSE
    Serial.print(unit, DEC);
#endif

    digitalWrite(13, HIGH);
    delay(unit);
    digitalWrite(13, LOW);
}

// --------------------
// delay 1 unit
static void do_unit_delay()
{
    delay(unitgap);

#if VERBOSE
    Serial.print(" ");
#endif
}

// --------------------
// play a numeric character 0 - 9
static void play_numeric(uint8_t ch)
{
    uint16_t letter = ch - '0';
    print_char(ch);
    for (int i = 0; numeric[letter][i] != 0; ++i) {
        do_unit(numeric[letter][i]);
        do_unit_delay();
    }

#if VERBOSE
    Serial.println("-");
#endif
}

// --------------------
// play a alphabetic letter a - z
static void play_letter(uint8_t ch)
{
    uint16_t letter = ch - 'A';
    print_char(ch);
    for (int i = 0; alphabet[letter][i] != 0; ++i) {
        do_unit(alphabet[letter][i]);
        do_unit_delay();
    }

#if VERBOSE
    Serial.println("-");
#endif
}

// --------------------
// play a string of alphanumeric characters or space
static void play_string(char* str)
{
    for (char* p = str; *p != 0; ++p) {
        if (*p == ' ') {
            // it's a word gap
            print_wordgap();
            delay(wordgap);
        } else if (isAlpha(*p)) {
            // it's a letter
            char ch = toupper(*p);
            play_letter(ch);
            delay(lettergap);
        } else if (isdigit(*p)) {
            // it's a numeric digit
            play_numeric(*p);
            delay(lettergap);
        }
    }
}

// --------------------
//! Arduino main loop
//! print SOS
// cppcheck-suppress unusedFunction
void loop()
{
    char* p = buffer;
    play_string(p);

    Serial.println("-");
    delay(1000);

    // loads the buffer with a string the user typed in
    int index = 0;
    while (Serial.available() > 0) {
        int ch = Serial.read();
        if (isAlpha(ch)) {
            buffer[index] = ch;
            buffer[++index] = 0;
        } else if (isdigit(ch)) {
            buffer[index] = ch;
            buffer[++index] = 0;
        } else if (ch == ' ') {
            buffer[index] = ch;
            buffer[++index] = 0;
        } else if (ch == 0x0A || ch == 0x0D) {
            // skip
        }
        // note: skip invalid characters as well
    }

    if (index == 0) {
        // user didn't type anything in, an empty string (i.e. just a newline)
        // then use default string
        strcpy(buffer, "SOS");
    }
}
